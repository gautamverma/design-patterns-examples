package com.example.observer;

/**
 * It is interface implemented by all observer and Subject call the update
 * method when its state changed. It ensures
 * 		* Classes are loosely coupled i.e know little about each other
 * 		* Changes in either of them doesn't break the other
 * @author Gautam 
 *
 */
public interface Observer 
{
	void update(double temp, double pressure, int humidity);
}
