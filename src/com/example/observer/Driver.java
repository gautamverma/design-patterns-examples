package com.example.observer;

public class Driver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		WeatherData weather  = new WeatherData();
		Observer cTemp = new CurrentTempDisplay();
		Observer yTemp = new ForecastWeatherDisplay();
		 
		weather.addObserver(yTemp);
		weather.addObserver(cTemp);
		
		for(int i = 0; i<10; i++)
		{
			weather.mesurementChanged();
			if(i==5)
				weather.removeObserver(yTemp);
		}

	}

}
