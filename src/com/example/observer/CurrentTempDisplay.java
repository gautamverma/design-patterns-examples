package com.example.observer;

/**
 * A concreate observer which is updated by Subject WeatherData 
 * @author Gautam
 *
 */
public class CurrentTempDisplay implements Observer {

	@Override
	public void update(double temp, double pressure, int humidity) {
		System.out.println("The Current Values T:"+temp+" P:"+pressure+" H:"+humidity);
	}

}
