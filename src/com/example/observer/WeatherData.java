package com.example.observer;

import java.util.ArrayList;
import java.util.List;

public class WeatherData implements Subject 
{
	List<Observer> observers;
	
	public WeatherData()
	{
		observers = new ArrayList<Observer>();
	}
	/**
	 * It is called when the weather values changed
	 */
	public void mesurementChanged()
	{
		// save the new values
		notifyObservers();
	}

	@Override
	public void addObserver(Observer o) 
	{
		observers.add(o);
	}

	@Override
	public boolean removeObserver(Observer o) 
	{
		return observers.remove(o);
	}

	@Override
	public void notifyObservers() 
	{
		for(int i = 0; i<observers.size(); i++)
		{
			observers.get(i).update(getTemp(), getPressure(), getHumidity());
		}
	}
	
	double getTemp()
	{
		return 58;
	}
	
	double getPressure()
	{
		return 98.0;
	}
	
	int getHumidity()
	{
		return 67;
	}
}
