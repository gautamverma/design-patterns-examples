package com.example.observer;

public class ForecastWeatherDisplay implements Observer {

	@Override
	public void update(double temp, double pressure, int humidity) 
	{
		System.out.println("The Coming T:P:H are "+temp+":"+pressure+":"+humidity);

	}

}
