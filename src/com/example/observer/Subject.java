package com.example.observer;

/**
 * Here we are building this pattern with push methodology i.e Subject push the changed
 * state values to the observers.
 * 
 * We can build this same pattern with pull startegy i.e pass the Subject as parameter
 * to the update method and let each observer decide what it want to pull from it.
 * ( java.util.observer supports both push and pull )
 * 
 * Java swing also user observer as the all the GUI Elements register listener i.e actually 
 * behind scenes are just like observer, which get notified when a event occurs.
 * @author Gautam
 *
 */
public interface Subject 
{
	void addObserver(Observer o);
	boolean removeObserver(Observer o);
	void notifyObservers();
}
