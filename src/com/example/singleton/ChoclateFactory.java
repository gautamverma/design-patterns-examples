package com.example.singleton;

/**
 * Though this class demonstrate the way of Singleton pattern
 * All the techniques to implement it in the java
 * 
 *  Singleton pattern - Allows only one instance of a class and provides a 
 *    global point to access it.
 *
 * @author Gautam
 *
 */
public class ChoclateFactory {

	boolean empty;
	boolean boiled;
	
	// Volatile keyword is used so that all thread take its correct value
	private volatile static ChoclateFactory instance;
	
	// Setting the Boiled to false and empty to true at start
	private ChoclateFactory()
	{
		empty = true;
		boiled = true;
	}

	/**
	 * Using double check lock
	 *  By this we get lazy initialization and also the synchronization 
	 *  overhead is reduced greatly
	 *  
	 *  Other methods are
	 *     synchronized the getInstance method **Large overhead possible
	 *     initialize the instance variable at declaration **Unnecessary overhead at startup
	 * @return
	 */
	public static ChoclateFactory getInstance()
	{
		if(instance==null)
		{
			synchronized (ChoclateFactory.class) 
			{
				instance = new ChoclateFactory();
			}
		}
		return instance;
	}
	
	public void fill()
	{
		System.out.print("Filling factory");
	}
	
	public void drain()
	{
		System.out.print("Emptying factory");
	}
	
	public boolean empty()
	{
		return empty;
	}
}
