package com.example.decorator;

public class HouseBlend extends Beverage {

	@Override
	public float cost() {
		return 0.89f;
	}

	public String getDescription()
	{
		return "HouseBlend";
	}
}
