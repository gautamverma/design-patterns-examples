package com.example.decorator;

public class DarkRoast extends Beverage 
{

	@Override
	public float cost() {
		return 0.99f;
	}

	public String getDescription()
	{
		return "DarkRoast";
	}
}
