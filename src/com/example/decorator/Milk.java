package com.example.decorator;

public class Milk extends Decorator 
{
	public Milk(Beverage beverage)
	{
		this.bevrege = beverage;
	}
	
	@Override
	public float cost() 
	{
		return 0.10f + bevrege.cost();
	}

	public String getDescription()
	{
		return bevrege.getDescription() + Beverage.SPACE + "Milk";
	}
}
