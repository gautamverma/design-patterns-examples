package com.example.decorator;

/*
 * The Base class of the all the drinks served in the House
 * 
 * Rather than making 1 class for each drink served we have made classes for 
 * each base drink(Decaf, Expresso, DarkRoast, HouseBlend) and decorator for each add on. 
 * So when add on are added they just decorate the base drink.
 * 
 * We calculate the cost by delegating the cost to each item added and base drink 
 */
public abstract class Beverage 
{
	public static String SPACE = " ";
	public abstract float cost();
	public abstract String getDescription();
}
