package com.example.decorator;

public class Mocha extends Decorator {

	public Mocha(Beverage beverage)
	{
		this.bevrege = beverage;
	}
	
	@Override
	public float cost() 
	{
		return	0.20f + bevrege.cost();
	}
	
	public String getDescription()
	{
		return bevrege.getDescription() + Beverage.SPACE + "Mocha";
	}

}
