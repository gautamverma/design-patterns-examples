package com.example.decorator;

public class Soy extends Decorator {

	public Soy(Beverage beverage)
	{
		this.bevrege = beverage;
	}
	
	@Override
	public float cost() 
	{
		return 	0.15f + bevrege.cost();
	}

	public String getDescription()
	{
		return bevrege.getDescription() + Beverage.SPACE + "Soy";
	}
}
