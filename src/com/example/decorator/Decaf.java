package com.example.decorator;

public class Decaf extends Beverage 
{

	@Override
	public float cost() {
		return 1.05f;
	}
	
	public String getDescription()
	{
		return "Decaf";
	}

}
