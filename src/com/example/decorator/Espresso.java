package com.example.decorator;

public class Espresso extends Beverage {

	@Override
	public float cost() {
		return 1.99f;
	}
	
	public String getDescription()
	{
		return "Espresso";
	}

}
