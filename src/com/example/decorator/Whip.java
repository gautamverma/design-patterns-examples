package com.example.decorator;

public class Whip extends Decorator {

	public Whip(Beverage beverage)
	{
		this.bevrege = beverage;
	}
	
	@Override
	public float cost() 
	{
		return 0.10f + bevrege.cost();
	}
	
	public String getDescription()
	{
		return bevrege.getDescription() + Beverage.SPACE + "Whip";
	}

}
